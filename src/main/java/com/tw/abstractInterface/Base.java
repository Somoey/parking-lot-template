package com.tw.abstractInterface;

public abstract class Base {
    abstract void print();

    public Base() {
        this.print();
    }
}
