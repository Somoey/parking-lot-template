package com.tw.functionalProgramming;

import lombok.Getter;

import java.util.List;

@Getter
public class Album {
    private String name;
    private List<Track> tracks;
    private List<Artist> musicians;

    public Album(String name, List<Track> tracks, List<Artist> musicians) {
        this.name = name;
        this.tracks = tracks;
        this.musicians = musicians;
    }
}
