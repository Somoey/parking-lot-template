package com.tw.functionalProgramming;

public class Track {
    private String name;

    public Track(String name) {
        this.name = name;
    }

    public int getLength() {
        return this.name.length();
    }
}
