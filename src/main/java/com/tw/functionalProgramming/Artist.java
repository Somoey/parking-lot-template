package com.tw.functionalProgramming;

import java.util.List;

public class Artist {
    private String name;
    private List<String>  members;
    private String origin;

    public Artist(String name, List<String> members, String origin) {
        this.name = name;
        this.members = members;
        this.origin = origin;
    }
}
