package com.tw.functionalProgramming;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TrackTest {
    @Test
    void should_get_shortest_track() {
        List<Track> tracks = Arrays.asList(new Track("A"), new Track("BB"), new Track("CCC"));
        Track shortestTrack = tracks.stream().min(Comparator.comparing(Track::getLength)).get();
        assertEquals(tracks.get(0), shortestTrack);
    }

    @Test
    void should_count_with_reduce() {
        int count = Stream.of(1, 2, 3)
                .reduce(0, Integer::sum);

        assertEquals(6, count);
    }
}
